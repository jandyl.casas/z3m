FROM node:10.16.3-alpine as build-stage
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY ./ .
RUN npm run build

FROM staticfloat/nginx-certbot as production-stage
RUN mkdir /app
COPY --from=build-stage /app/dist /app
COPY /etc/nginx.conf /etc/nginx/conf.d/
EXPOSE 80
EXPOSE 443
ENV CERTBOT_EMAIL=z3mcarwashing@gmail.com
ENV IS_STAGING=0
