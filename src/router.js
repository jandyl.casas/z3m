// routes.js

import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

function load (componentName){
    return () => import(`@/components/${componentName}`)
}

const router = new VueRouter({
    mode: 'history',
    routes:
    [
        { path: '/home', component: load('Home'), name: 'home', alias: '/'},
        { path: '/about', component: load('AboutUs'), name: 'about' },
        { path: '/contact', component: load('ContactUs'), name: 'contact' },
        { path: '/services/distribution', component: load('Distribution'), name: 'distribution' },
        { path: '/services/professional_car_grooming_services', component: load('Grooming'), name: 'grooming' },
        { path: '/services/academy', component: load('Academy'), name: 'academy' },
        { path: '/services', name: 'services' },
        { path: '*', component: load('404'), name: '404'}
    ],   
    scrollBehavior() {
    document.getElementById('app').scrollIntoView();
    }
})

export default router;